<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:bproc="http://schemas.haulmont.com/bproc/bpmn" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.flowable.org/processdef">
  <collaboration id="Collaboration_1b1jcst">
    <participant id="Participant_0o7akr2" processRef="contractApproval" />
  </collaboration>
  <process id="contractApproval" name="Contract approval" isExecutable="true">
    <documentation>Contract approval process</documentation>
    <laneSet id="LaneSet_0brr4rk">
      <lane id="Lane_0tojgo3" name="Initiator">
        <extensionElements>
          <bproc:assignmentDetails assigneeSource="userProvider" assigneeValue="manager_NewUserProvider" assignee="${manager_NewUserProvider.getForProcess(execution.id)}" candidateUsersSource="users" candidateGroups="" candidateGroupsValue="" candidateGroupsSource="userGroups" />
        </extensionElements>
        <flowNodeRef>Task_0s7ocki</flowNodeRef>
        <flowNodeRef>EndEvent_0p2bdpg</flowNodeRef>
        <flowNodeRef>approval</flowNodeRef>
        <flowNodeRef>ExclusiveGateway_1iwxocn</flowNodeRef>
        <flowNodeRef>Task_14zck4c</flowNodeRef>
        <flowNodeRef>EndEvent_1vdxkhx</flowNodeRef>
        <flowNodeRef>Task_00z86i1</flowNodeRef>
        <flowNodeRef>Task_1f28fpm</flowNodeRef>
        <flowNodeRef>ExclusiveGateway_158pbuv</flowNodeRef>
        <flowNodeRef>Task_17xehmv</flowNodeRef>
        <flowNodeRef>new-contract</flowNodeRef>
      </lane>
      <lane id="Lane_0jo8ehr">
        <extensionElements>
          <bproc:assignmentDetails assigneeSource="userProvider" candidateUsersSource="users" candidateGroupsSource="userGroups" />
        </extensionElements>
      </lane>
    </laneSet>
    <sequenceFlow id="SequenceFlow_1bsjn8g" name="approved" sourceRef="ExclusiveGateway_1iwxocn" targetRef="Task_00z86i1">
      <extensionElements>
        <bproc:conditionDetails conditionSource="userTaskOutcome" conditionType="anyoneCompleted" userTaskId="approval" userTaskOutcome="approve" />
      </extensionElements>
      <conditionExpression xsi:type="tFormalExpression">${execution.getVariable('approval_result') != null &amp;&amp; bproc_UserTaskResults.containsOutcome(execution.getVariable('approval_result'), 'approve')}</conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_0upfeuy" sourceRef="approval" targetRef="ExclusiveGateway_1iwxocn">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" />
      </extensionElements>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_04gv0d4" name="onApproval" sourceRef="Task_0s7ocki" targetRef="approval">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" conditionType="anyoneCompleted" />
      </extensionElements>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_0wv5ag5" name="rejected" sourceRef="ExclusiveGateway_1iwxocn" targetRef="Task_14zck4c">
      <extensionElements>
        <bproc:conditionDetails conditionSource="userTaskOutcome" conditionType="anyoneCompleted" userTaskId="approval" userTaskOutcome="reject" />
      </extensionElements>
      <conditionExpression xsi:type="tFormalExpression">${execution.getVariable('approval_result') != null &amp;&amp; bproc_UserTaskResults.containsOutcome(execution.getVariable('approval_result'), 'reject')}</conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_1c6z4b9" name="canceled" sourceRef="Task_14zck4c" targetRef="EndEvent_1vdxkhx">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" />
      </extensionElements>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_1qxy0ll" name="completion" sourceRef="ExclusiveGateway_158pbuv" targetRef="Task_17xehmv">
      <extensionElements>
        <bproc:conditionDetails conditionSource="userTaskOutcome" conditionType="anyoneCompleted" userTaskId="Task_1f28fpm" userTaskOutcome="complete" />
      </extensionElements>
      <conditionExpression xsi:type="tFormalExpression">${execution.getVariable('Task_1f28fpm_result') != null &amp;&amp; bproc_UserTaskResults.containsOutcome(execution.getVariable('Task_1f28fpm_result'), 'complete')}</conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_1aq91dn" name="completed" sourceRef="Task_17xehmv" targetRef="EndEvent_0p2bdpg">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" />
      </extensionElements>
    </sequenceFlow>
    <serviceTask id="Task_0s7ocki" name="Set the on approval status" flowable:expression="${manager_ContractStatusService.setStatus(contract, &#39;OnApproval&#39;)}" bproc:taskType="springBean">
      <extensionElements>
        <bproc:springBean beanName="manager_ContractStatusService" methodName="setStatus">
          <bproc:methodParam name="contract" type="com.company.manager.entity.Contract" isVariable="true">contract</bproc:methodParam>
          <bproc:methodParam name="status" type="java.lang.String" isVariable="false">OnApproval</bproc:methodParam>
        </bproc:springBean>
      </extensionElements>
      <incoming>SequenceFlow_1iw5s74</incoming>
      <outgoing>SequenceFlow_04gv0d4</outgoing>
    </serviceTask>
    <sequenceFlow id="SequenceFlow_03z3e4o" name="active" sourceRef="Task_00z86i1" targetRef="Task_1f28fpm">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" />
      </extensionElements>
    </sequenceFlow>
    <sequenceFlow id="SequenceFlow_1f66lsh" sourceRef="Task_1f28fpm" targetRef="ExclusiveGateway_158pbuv" />
    <sequenceFlow id="SequenceFlow_1e4vudi" name="cancel completion" sourceRef="ExclusiveGateway_158pbuv" targetRef="Task_1f28fpm">
      <extensionElements>
        <bproc:conditionDetails conditionSource="userTaskOutcome" conditionType="anyoneCompleted" userTaskId="Task_1f28fpm" userTaskOutcome="cancel" />
      </extensionElements>
      <conditionExpression xsi:type="tFormalExpression">${execution.getVariable('Task_1f28fpm_result') != null &amp;&amp; bproc_UserTaskResults.containsOutcome(execution.getVariable('Task_1f28fpm_result'), 'cancel')}</conditionExpression>
    </sequenceFlow>
    <endEvent id="EndEvent_0p2bdpg">
      <incoming>SequenceFlow_1aq91dn</incoming>
    </endEvent>
    <userTask id="approval" name="Approval the Contract" flowable:candidateUsers="" bproc:assigneeSource="expression" bproc:candidateUsersValue="">
      <extensionElements>
        <bproc:formData type="input-dialog" openMode="DIALOG">
          <bproc:formFields>
            <bproc:formField id="contract" caption="Contract" type="entity" editable="false" required="false">
              <bproc:formFieldProperty name="uiComponent" value="pickerField" />
              <bproc:formFieldProperty name="entityName" value="manager_Contract" />
            </bproc:formField>
          </bproc:formFields>
          <bproc:formOutcomes>
            <bproc:formOutcome id="approve" caption="Approve" icon="font-icon:CHECK" />
            <bproc:formOutcome id="reject" caption="Reject" icon="font-icon:BAN" />
          </bproc:formOutcomes>
        </bproc:formData>
      </extensionElements>
      <incoming>SequenceFlow_04gv0d4</incoming>
      <outgoing>SequenceFlow_0upfeuy</outgoing>
    </userTask>
    <exclusiveGateway id="ExclusiveGateway_1iwxocn">
      <incoming>SequenceFlow_0upfeuy</incoming>
      <outgoing>SequenceFlow_0wv5ag5</outgoing>
      <outgoing>SequenceFlow_1bsjn8g</outgoing>
    </exclusiveGateway>
    <serviceTask id="Task_14zck4c" name="Set the canceled status" flowable:expression="${manager_ContractStatusService.setStatus(contract, &#39;Canceled&#39;)}" bproc:taskType="springBean">
      <extensionElements>
        <bproc:springBean beanName="manager_ContractStatusService" methodName="setStatus">
          <bproc:methodParam name="contract" type="com.company.manager.entity.Contract" isVariable="true">contract</bproc:methodParam>
          <bproc:methodParam name="status" type="java.lang.String" isVariable="false">Canceled</bproc:methodParam>
        </bproc:springBean>
      </extensionElements>
      <incoming>SequenceFlow_0wv5ag5</incoming>
      <outgoing>SequenceFlow_1c6z4b9</outgoing>
    </serviceTask>
    <endEvent id="EndEvent_1vdxkhx">
      <incoming>SequenceFlow_1c6z4b9</incoming>
    </endEvent>
    <serviceTask id="Task_00z86i1" name="Set the active status" flowable:expression="${manager_ContractStatusService.setStatus(contract, &#39;Active&#39;)}" bproc:taskType="springBean">
      <extensionElements>
        <bproc:springBean beanName="manager_ContractStatusService" methodName="setStatus">
          <bproc:methodParam name="contract" type="com.company.manager.entity.Contract" isVariable="true">contract</bproc:methodParam>
          <bproc:methodParam name="status" type="java.lang.String" isVariable="false">Active</bproc:methodParam>
        </bproc:springBean>
      </extensionElements>
      <incoming>SequenceFlow_1bsjn8g</incoming>
      <outgoing>SequenceFlow_03z3e4o</outgoing>
    </serviceTask>
    <userTask id="Task_1f28fpm" name="Completion the Contract" bproc:assigneeSource="expression">
      <extensionElements>
        <bproc:formData type="input-dialog" openMode="DIALOG">
          <bproc:formFields>
            <bproc:formField id="contract" caption="Contract" type="entity" editable="false" required="false">
              <bproc:formFieldProperty name="uiComponent" value="pickerField" />
              <bproc:formFieldProperty name="entityName" value="manager_Contract" />
            </bproc:formField>
          </bproc:formFields>
          <bproc:formOutcomes>
            <bproc:formOutcome id="complete" caption="Complete" icon="font-icon:CHECK" />
            <bproc:formOutcome id="cancel" caption="Cancel" icon="font-icon:BAN" />
          </bproc:formOutcomes>
        </bproc:formData>
      </extensionElements>
      <incoming>SequenceFlow_03z3e4o</incoming>
      <incoming>SequenceFlow_1e4vudi</incoming>
      <outgoing>SequenceFlow_1f66lsh</outgoing>
    </userTask>
    <exclusiveGateway id="ExclusiveGateway_158pbuv">
      <incoming>SequenceFlow_1f66lsh</incoming>
      <outgoing>SequenceFlow_1qxy0ll</outgoing>
      <outgoing>SequenceFlow_1e4vudi</outgoing>
    </exclusiveGateway>
    <serviceTask id="Task_17xehmv" name="Set the completed status" flowable:expression="${manager_ContractStatusService.setStatus(contract, &#39;Completed&#39;)}" bproc:taskType="springBean">
      <extensionElements>
        <bproc:springBean beanName="manager_ContractStatusService" methodName="setStatus">
          <bproc:methodParam name="contract" type="com.company.manager.entity.Contract" isVariable="true">contract</bproc:methodParam>
          <bproc:methodParam name="status" type="java.lang.String" isVariable="false">Completed</bproc:methodParam>
        </bproc:springBean>
      </extensionElements>
      <incoming>SequenceFlow_1qxy0ll</incoming>
      <outgoing>SequenceFlow_1aq91dn</outgoing>
    </serviceTask>
    <sequenceFlow id="SequenceFlow_1iw5s74" sourceRef="new-contract" targetRef="Task_0s7ocki">
      <extensionElements>
        <bproc:conditionDetails conditionSource="expression" />
      </extensionElements>
    </sequenceFlow>
    <startEvent id="new-contract" name="NewContract">
      <extensionElements>
        <bproc:formData type="no-form" />
        <bproc:processVariables>
          <bproc:processVariable name="contract" type="entity" />
          <bproc:processVariable name="administrator" type="user" />
        </bproc:processVariables>
      </extensionElements>
      <outgoing>SequenceFlow_1iw5s74</outgoing>
    </startEvent>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_process">
    <bpmndi:BPMNPlane id="BPMNPlane_process" bpmnElement="Collaboration_1b1jcst">
      <bpmndi:BPMNShape id="Participant_0o7akr2_di" bpmnElement="Participant_0o7akr2" isHorizontal="true">
        <omgdc:Bounds x="100" y="105" width="1279" height="619" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="BPMNShape_startEvent1" bpmnElement="new-contract">
        <omgdc:Bounds x="153" y="150" width="30" height="30" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="144" y="127" width="65" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_1iwxocn_di" bpmnElement="ExclusiveGateway_1iwxocn" isMarkerVisible="true">
        <omgdc:Bounds x="564" y="140" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="UserTask_1vgtf3j_di" bpmnElement="approval">
        <omgdc:Bounds x="431" y="125" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="UserTask_0armg1u_di" bpmnElement="Task_1f28fpm">
        <omgdc:Bounds x="870" y="125" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1bsjn8g_di" bpmnElement="SequenceFlow_1bsjn8g">
        <omgdi:waypoint x="614" y="165" />
        <omgdi:waypoint x="694" y="165" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="615" y="153.00000000000006" width="47" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0upfeuy_di" bpmnElement="SequenceFlow_0upfeuy">
        <omgdi:waypoint x="531" y="165" />
        <omgdi:waypoint x="564" y="165" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_04gv0d4_di" bpmnElement="SequenceFlow_04gv0d4">
        <omgdi:waypoint x="339" y="165" />
        <omgdi:waypoint x="431" y="165" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="357" y="147" width="57" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Lane_0tojgo3_di" bpmnElement="Lane_0tojgo3" isHorizontal="true">
        <omgdc:Bounds x="130" y="105" width="1249" height="463" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Lane_0jo8ehr_di" bpmnElement="Lane_0jo8ehr" isHorizontal="true">
        <omgdc:Bounds x="130" y="568" width="1249" height="156" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0wv5ag5_di" bpmnElement="SequenceFlow_0wv5ag5">
        <omgdi:waypoint x="589" y="190" />
        <omgdi:waypoint x="589" y="266" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="549" y="195" width="40" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="ServiceTask_0hb4vt3_di" bpmnElement="Task_14zck4c">
        <omgdc:Bounds x="539" y="266" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_1vdxkhx_di" bpmnElement="EndEvent_1vdxkhx">
        <omgdc:Bounds x="571" y="399" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1c6z4b9_di" bpmnElement="SequenceFlow_1c6z4b9">
        <omgdi:waypoint x="589" y="346" />
        <omgdi:waypoint x="589" y="399" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="605" y="351" width="45" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1qxy0ll_di" bpmnElement="SequenceFlow_1qxy0ll">
        <omgdi:waypoint x="1052" y="165" />
        <omgdi:waypoint x="1131" y="165" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="1066" y="147" width="52" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="ServiceTask_0vqc3se_di" bpmnElement="Task_17xehmv">
        <omgdc:Bounds x="1131" y="125" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_0p2bdpg_di" bpmnElement="EndEvent_0p2bdpg">
        <omgdc:Bounds x="1323" y="147" width="36" height="36" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1aq91dn_di" bpmnElement="SequenceFlow_1aq91dn">
        <omgdi:waypoint x="1231" y="165" />
        <omgdi:waypoint x="1323" y="165" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="1252" y="147" width="50" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="ServiceTask_0f43jwq_di" bpmnElement="Task_0s7ocki">
        <omgdc:Bounds x="239" y="125" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ServiceTask_09sfibn_di" bpmnElement="Task_00z86i1">
        <omgdc:Bounds x="694" y="125" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_03z3e4o_di" bpmnElement="SequenceFlow_03z3e4o">
        <omgdi:waypoint x="794" y="165" />
        <omgdi:waypoint x="870" y="165" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="817" y="147" width="30" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="ExclusiveGateway_158pbuv_di" bpmnElement="ExclusiveGateway_158pbuv" isMarkerVisible="true">
        <omgdc:Bounds x="1002" y="140" width="50" height="50" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1f66lsh_di" bpmnElement="SequenceFlow_1f66lsh">
        <omgdi:waypoint x="970" y="165" />
        <omgdi:waypoint x="1002" y="165" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1e4vudi_di" bpmnElement="SequenceFlow_1e4vudi">
        <omgdi:waypoint x="1027" y="190" />
        <omgdi:waypoint x="1027" y="296" />
        <omgdi:waypoint x="920" y="296" />
        <omgdi:waypoint x="920" y="205" />
        <bpmndi:BPMNLabel>
          <omgdc:Bounds x="931" y="278" width="87" height="14" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1iw5s74_di" bpmnElement="SequenceFlow_1iw5s74">
        <omgdi:waypoint x="183" y="165" />
        <omgdi:waypoint x="239" y="165" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
