package com.company.manager.web.screens.invoice;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Invoice;

@UiController("manager_Invoice.edit")
@UiDescriptor("invoice-edit.xml")
@EditedEntityContainer("invoiceDc")
@LoadDataBeforeShow
public class InvoiceEdit extends StandardEditor<Invoice> {
}