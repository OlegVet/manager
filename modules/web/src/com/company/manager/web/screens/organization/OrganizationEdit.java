package com.company.manager.web.screens.organization;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Organization;

@UiController("manager_Organization.edit")
@UiDescriptor("organization-edit.xml")
@EditedEntityContainer("organizationDc")
@LoadDataBeforeShow
public class OrganizationEdit extends StandardEditor<Organization> {
}