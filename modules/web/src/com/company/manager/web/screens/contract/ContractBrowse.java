package com.company.manager.web.screens.contract;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Contract;

@UiController("manager_Contract.browse")
@UiDescriptor("contract-browse.xml")
@LookupComponent("contractsTable")
@LoadDataBeforeShow
public class ContractBrowse extends StandardLookup<Contract> {
}