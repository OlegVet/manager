package com.company.manager.web.screens.stage;

import com.company.manager.config.VatConfig;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Stage;

import javax.inject.Inject;
import java.math.BigDecimal;

@UiController("manager_Stage.edit")
@UiDescriptor("stage-edit.xml")
@EditedEntityContainer("stageDc")
@LoadDataBeforeShow
public class StageEdit extends StandardEditor<Stage> {
    @Inject
    private VatConfig vatConfig;

    @Subscribe("amountField")
    public void onAmountFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
        BigDecimal vat = getEditedEntity().getContract().calculateVat(getEditedEntity().getAmount(), vatConfig.getVat());
        if (vat != null) {
            getEditedEntity().setVat(vat);
            getEditedEntity().setTotalAmount(getEditedEntity().getAmount().add(vat));
        }
    }
}