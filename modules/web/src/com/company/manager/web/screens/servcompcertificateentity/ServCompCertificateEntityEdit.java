package com.company.manager.web.screens.servcompcertificateentity;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.ServCompCertificateEntity;

@UiController("manager_ServCompCertificateEntity.edit")
@UiDescriptor("serv-comp-certificate-entity-edit.xml")
@EditedEntityContainer("servCompCertificateEntityDc")
@LoadDataBeforeShow
public class ServCompCertificateEntityEdit extends StandardEditor<ServCompCertificateEntity> {
}