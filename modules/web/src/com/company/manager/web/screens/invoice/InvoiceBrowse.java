package com.company.manager.web.screens.invoice;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Invoice;

@UiController("manager_Invoice.browse")
@UiDescriptor("invoice-browse.xml")
@LookupComponent("invoicesTable")
@LoadDataBeforeShow
public class InvoiceBrowse extends StandardLookup<Invoice> {
}