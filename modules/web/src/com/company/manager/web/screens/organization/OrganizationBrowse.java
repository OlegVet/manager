package com.company.manager.web.screens.organization;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Organization;

@UiController("manager_Organization.browse")
@UiDescriptor("organization-browse.xml")
@LookupComponent("organizationsTable")
@LoadDataBeforeShow
public class OrganizationBrowse extends StandardLookup<Organization> {
}