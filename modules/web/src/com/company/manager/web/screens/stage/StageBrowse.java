package com.company.manager.web.screens.stage;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.Stage;

@UiController("manager_Stage.browse")
@UiDescriptor("stage-browse.xml")
@LookupComponent("stagesTable")
@LoadDataBeforeShow
public class StageBrowse extends StandardLookup<Stage> {
}