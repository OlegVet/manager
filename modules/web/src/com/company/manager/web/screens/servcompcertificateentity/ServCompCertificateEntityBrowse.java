package com.company.manager.web.screens.servcompcertificateentity;

import com.haulmont.cuba.gui.screen.*;
import com.company.manager.entity.ServCompCertificateEntity;

@UiController("manager_ServCompCertificateEntity.browse")
@UiDescriptor("serv-comp-certificate-entity-browse.xml")
@LookupComponent("servCompCertificateEntitiesTable")
@LoadDataBeforeShow
public class ServCompCertificateEntityBrowse extends StandardLookup<ServCompCertificateEntity> {
}