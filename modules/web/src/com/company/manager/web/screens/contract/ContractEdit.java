package com.company.manager.web.screens.contract;

import com.company.manager.config.VatConfig;
import com.company.manager.entity.*;
import com.company.manager.web.screens.invoice.InvoiceEdit;
import com.company.manager.web.screens.servcompcertificateentity.ServCompCertificateEntityEdit;
import com.haulmont.addon.bproc.service.BprocRuntimeService;
import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@UiController("manager_Contract.edit")
@UiDescriptor("contract-edit.xml")
@EditedEntityContainer("contractDc")
@LoadDataBeforeShow
public class ContractEdit extends StandardEditor<Contract> {
    private static final String PROCESS_DEFINITION_KEY = "contractApproval";
    private static final String CONTRACT_STATUS_NEW = "New";

    private boolean isContractNew;

    private static final Logger log = LoggerFactory.getLogger(ContractEdit.class);
    @Inject
    private VatConfig vatConfig;
    @Inject
    private Notifications notifications;
    @Inject
    private ScreenBuilders screenBuilders;
    @Inject
    private Table<Stage> stageTable;
    @Inject
    private UserSession userSession;
    @Inject
    private BprocRuntimeService bprocRuntimeService;
    @Inject
    private MetadataTools metadataTools;

    @Subscribe
    public void onInitEntity(InitEntityEvent<Contract> event) {
        isContractNew = true;
        event.getEntity().setStatus(ContractStatus.fromId(CONTRACT_STATUS_NEW));
    }

    @Subscribe("amountField")
    public void onAmountFieldValueChange(HasValue.ValueChangeEvent<BigDecimal> event) {
        BigDecimal vat = getEditedEntity().calculateVat(getEditedEntity().getAmount(), vatConfig.getVat());
        if (vat != null) {
            getEditedEntity().setVat(vat);
            getEditedEntity().setTotalAmount(getEditedEntity().getAmount().add(vat));
        }
    }

    @Subscribe("customerField")
    public void onCustomerFieldValueChange(HasValue.ValueChangeEvent<Organization> event) {
        BigDecimal vat = getEditedEntity().calculateVat(getEditedEntity().getAmount(), vatConfig.getVat());
        if (vat != null) {
            getEditedEntity().setVat(vat);
            getEditedEntity().setTotalAmount(getEditedEntity().getAmount().add(vat));
        }
    }

    @Subscribe("performerField")
    public void onPerformerFieldValueChange(HasValue.ValueChangeEvent<Organization> event) {
        BigDecimal vat = getEditedEntity().calculateVat(getEditedEntity().getAmount(), vatConfig.getVat());
        if (vat != null) {
            getEditedEntity().setVat(vat);
            getEditedEntity().setTotalAmount(getEditedEntity().getAmount().add(vat));
        }
    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {

    }

    @SuppressWarnings("unused")
    @Nullable
    public BigDecimal calculateVat(BigDecimal amount) {

        if (amount == null) return null;

        boolean escapeVat = false;
        if ((getEditedEntity().getCustomer() != null) && getEditedEntity().getCustomer().getEscapeVat())
            escapeVat = true;
        if ((getEditedEntity().getPerformer() != null) && getEditedEntity().getPerformer().getEscapeVat())
            escapeVat = true;

        BigDecimal vatInPercents;
        if (escapeVat) {
            vatInPercents = BigDecimal.valueOf(0); // Organization escape vat
        } else {
            try {
                vatInPercents = BigDecimal.valueOf(Integer.parseInt(vatConfig.getVat()));
            } catch (NumberFormatException ignore) {
                vatInPercents = BigDecimal.valueOf(13); // If vat_in_% in config is not valid number
            }
        }

        return amount.multiply(vatInPercents).divide(BigDecimal.valueOf(100), BigDecimal.ROUND_HALF_UP);
    }

    @Subscribe("createInvoiceButton")
    public void onCreateInvoiceButtonClick(Button.ClickEvent event) {
        log.info("create invoice");
        Stage selectedStage = stageTable.getSingleSelected();
        if (selectedStage == null) {
            log.info("Stage not selected");
            notifications.create(Notifications.NotificationType.HUMANIZED).withCaption("Этап не выбран!").show();
            return;
        }

        Screen invoiceEditor = screenBuilders.editor(Invoice.class, this)
                .newEntity()
                .withInitializer(invoice -> {          // lambda to initialize new instance
                    invoice.getStageBase().setDate(selectedStage.getDateFrom());
                    invoice.getStageBase().setAmount(selectedStage.getAmount());
                    invoice.getStageBase().setVat(selectedStage.getVat());
                    invoice.getStageBase().setTotalAmount(selectedStage.getTotalAmount());
                    invoice.getStageBase().setDescription(selectedStage.getDescription());
                    invoice.setContract(selectedStage.getContract());
                })
                .withScreenClass(InvoiceEdit.class)    // specific editor screen
                .withOpenMode(OpenMode.DIALOG)         // open as modal dialog
                .build();

        invoiceEditor.addAfterCloseListener(afterCloseEvent -> {
            if (afterCloseEvent.closedWith(StandardOutcome.COMMIT)) {
                getScreenData().loadAll();
            }
        });

        invoiceEditor.show();
    }

    @Subscribe("createSscButton")
    public void onCreateSscButtonClick(Button.ClickEvent event) {
        log.info("create certificate");

        Stage selectedStage = stageTable.getSingleSelected();
        if (selectedStage == null) {
            log.info("Stage not selected");
            notifications.create(Notifications.NotificationType.HUMANIZED).withCaption("Этап не выбран!").show();
            return;
        }

        Screen certificateEditor = screenBuilders.editor(ServCompCertificateEntity.class, this)
                .newEntity()
                .withInitializer(certificateEntity -> {          // lambda to initialize new instance
                    certificateEntity.getStageBase().setDate(selectedStage.getDateFrom());
                    certificateEntity.getStageBase().setAmount(selectedStage.getAmount());
                    certificateEntity.getStageBase().setVat(selectedStage.getVat());
                    certificateEntity.getStageBase().setTotalAmount(selectedStage.getTotalAmount());
                    certificateEntity.getStageBase().setDescription(selectedStage.getDescription());
                    certificateEntity.setContract(selectedStage.getContract());
                })
                .withScreenClass(ServCompCertificateEntityEdit.class)    // specific editor screen
                .withOpenMode(OpenMode.DIALOG)         // open as modal dialog
                .build();

        certificateEditor.addAfterCloseListener(afterCloseEvent -> {
            if (afterCloseEvent.closedWith(StandardOutcome.COMMIT)) {
                getScreenData().loadAll();
            }
        });

        certificateEditor.show();
    }

    @Subscribe
    public void onAfterCommitChanges(AfterCommitChangesEvent event) {
        Contract contract = getEditedEntity();
        if (isContractNew) {
            Map<String, Object> processVariables = new HashMap<>();
            processVariables.put("contract", contract);
            processVariables.put("administrator", userSession.getCurrentOrSubstitutedUser());
            bprocRuntimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, metadataTools.getInstanceName(contract), processVariables);
        }
    }
}