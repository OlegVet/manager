package com.company.manager.service;

import com.company.manager.entity.Contract;
import com.company.manager.entity.Stage;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.TransactionalDataManager;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

@Service(StageService.NAME)
public class StageServiceBean implements StageService {

    @Inject
    private Persistence persistence;
    @Inject
    private Logger log;
    @Inject
    private TransactionalDataManager txDataManager;

    @Override
    public void create(Contract contract, List<Stage> stagesDc) {

        log.info("transaction is " + persistence.isInTransaction());
        Stage stage = txDataManager.create(Stage.class);
        stage.setName("Этап 1");
        stage.setDateFrom(contract.getDateFrom());
        stage.setDateTo(contract.getDateTo());
        stage.setAmount(contract.getAmount());
        stage.setVat(contract.getVat());
        stage.setTotalAmount(contract.getTotalAmount());
        stage.setDescription("Этап, включающий в себя всю работу по контракту. Создан автоматически.");
        stage.setContract(contract);
        contract.setStage(Collections.singletonList(stage));
        txDataManager.save(stage);

    }
}