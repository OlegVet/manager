package com.company.manager.service;

import com.company.manager.entity.Contract;
import com.company.manager.entity.ContractStatus;
import com.haulmont.cuba.core.app.EmailService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.EmailInfo;
import com.haulmont.cuba.core.global.EmailInfoBuilder;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.security.entity.User;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(ContractStatusService.NAME)
public class ContractStatusServiceBean implements ContractStatusService {
    private static final String MANAGER_ROLE_NAME = "manager";
    private static final String INITIATOR_ROLE_NAME = "initiator";

    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;
    @Inject
    private EmailService emailService;

    @Override
    public void setStatus(Contract contract, String status) {
        Contract reloadedContract = dataManager.reload(contract, View.LOCAL);
        reloadedContract.setStatus(ContractStatus.fromId(status));
        dataManager.commit(reloadedContract);
        sendEmail(contract, status);
    }


    private void sendEmail(Contract contract, String status) {
        List<User> managers = dataManager.load(User.class)
                .query("select u from sec$User u join u.userRoles ur where ur.roleName = :roleName or ur.roleName = :roleName2")
                .parameter("roleName", MANAGER_ROLE_NAME)
                .parameter("roleName2", INITIATOR_ROLE_NAME)
                .list();
        for (User user : managers) {

            if (user.getEmail() == null){
                log.info("Unable to send email for user " + user.getName() + ": no email address");
                continue;
            }
            EmailInfo emailInfo = EmailInfoBuilder.create()
                    .setAddresses(user.getEmail())
                    .setCaption("New contract status")
                    .setBody("A new status '" + status + "' has been set for the contract " + contract.getNumber() + ".")
                    .build();
            emailService.sendEmailAsync(emailInfo);
            log.info("Send email for " + emailInfo.getAddresses() + ": " + emailInfo.getBody());
        }
    }
}