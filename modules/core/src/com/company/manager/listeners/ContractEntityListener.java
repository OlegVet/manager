package com.company.manager.listeners;

import com.company.manager.entity.Contract;
import com.company.manager.entity.Stage;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.app.events.EntityChangedEvent;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.UUID;

@Component("manager_ContractEntityListener")
public class ContractEntityListener implements
        BeforeUpdateEntityListener<Contract>,
        BeforeInsertEntityListener<Contract> {

    @Inject
    private Metadata metadata;

    @Override
    public void onBeforeUpdate(Contract contract, EntityManager entityManager) {

        if (contract.getStage() != null) {
            if (contract.getStage().size() != 0) return; // Stages count > 0
        }

        // Merge customer instance because it comes to onBeforeInsert as part of another
        // entity's object graph and can be detached
        Stage stage = entityManager.merge(metadata.create(Stage.class));
        contract.setStage(Collections.singletonList(stage));

        // Set the stage. It will be saved on transaction commit.
        stage.setName("Этап #1");
        stage.setDateFrom(contract.getDateFrom());
        stage.setDateTo(contract.getDateTo());
        stage.setAmount(contract.getAmount());
        stage.setVat(contract.getVat());
        stage.setTotalAmount(contract.getTotalAmount());
        stage.setDescription("Этап, включающий в себя весь контракт. Создан автоматически.");
        stage.setContract(contract);
//        stagesDc.add(stage);
    }

    @Override
    public void onBeforeInsert(Contract contract, EntityManager entityManager) {
        onBeforeUpdate(contract, entityManager);
    }
}