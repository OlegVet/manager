package com.company.manager.core.role;

import com.company.manager.entity.*;
import com.haulmont.cuba.core.config.AppPropertyEntity;
import com.haulmont.cuba.security.app.role.AnnotatedRoleDefinition;
import com.haulmont.cuba.security.app.role.annotation.*;
import com.haulmont.cuba.security.entity.EntityOp;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.role.EntityAttributePermissionsContainer;
import com.haulmont.cuba.security.role.EntityPermissionsContainer;
import com.haulmont.cuba.security.role.ScreenPermissionsContainer;
import com.haulmont.cuba.security.role.SpecificPermissionsContainer;

@Role(name = InitiatorRole.NAME)
public class InitiatorRole extends AnnotatedRoleDefinition {
    public final static String NAME = "initiator";

    @EntityAccess(entityClass = AppPropertyEntity.class, operations = {EntityOp.CREATE, EntityOp.UPDATE, EntityOp.READ, EntityOp.DELETE})
    @EntityAccess(entityClass = Status.class, operations = {EntityOp.CREATE, EntityOp.UPDATE, EntityOp.READ, EntityOp.DELETE})
    @EntityAccess(entityClass = StageBase.class, operations = {EntityOp.CREATE, EntityOp.UPDATE, EntityOp.READ, EntityOp.DELETE})
    @EntityAccess(entityClass = Invoice.class, operations = {EntityOp.CREATE, EntityOp.UPDATE, EntityOp.READ, EntityOp.DELETE})
    @EntityAccess(entityClass = ServCompCertificateEntity.class, operations = {EntityOp.CREATE, EntityOp.UPDATE, EntityOp.READ, EntityOp.DELETE})
    @EntityAccess(entityClass = Contract.class, operations = {EntityOp.CREATE, EntityOp.READ, EntityOp.UPDATE, EntityOp.DELETE})
    @EntityAccess(entityClass = Organization.class, operations = {EntityOp.CREATE, EntityOp.READ, EntityOp.UPDATE, EntityOp.DELETE})
    @EntityAccess(entityClass = Stage.class, operations = {EntityOp.CREATE, EntityOp.READ, EntityOp.UPDATE, EntityOp.DELETE})
    @EntityAccess(entityClass = User.class, operations = {EntityOp.CREATE, EntityOp.READ, EntityOp.UPDATE, EntityOp.DELETE})
    @Override
    public EntityPermissionsContainer entityPermissions() {
        return super.entityPermissions();
    }

    @EntityAttributeAccess(entityClass = AppPropertyEntity.class, modify = "*")
    @EntityAttributeAccess(entityClass = Status.class, modify = "*")
    @EntityAttributeAccess(entityClass = StageBase.class, modify = "*")
    @EntityAttributeAccess(entityClass = Invoice.class, modify = "*")
    @EntityAttributeAccess(entityClass = ServCompCertificateEntity.class, modify = "*")
    @EntityAttributeAccess(entityClass = Contract.class, modify = "*")
    @EntityAttributeAccess(entityClass = Organization.class, modify = "*")
    @EntityAttributeAccess(entityClass = Stage.class, modify = "*")
    @EntityAttributeAccess(entityClass = User.class, modify = "*")
    @Override
    public EntityAttributePermissionsContainer entityAttributePermissions() {
        return super.entityAttributePermissions();
    }

    @ScreenAccess(screenIds = {"manager_Contract.edit", "manager_Contract.browse", "manager_Stage.edit", "manager_Stage.browse", "extMainScreen", "menu", "application-manager", "reports", "aboutWindow", "help", "settings", "mainWindow", "report$Report.browse", "report$ReportGroup.browse", "report$Report.run", "report$showChart", "report$showReportTable", "report$showPivotTable", "appProperties", "appPropertyEditor", "main", "manager_Invoice.browse", "manager_Organization.browse", "manager_Invoice.edit", "manager_Organization.edit", "manager_ServCompCertificateEntity.browse", "manager_ServCompCertificateEntity.edit", "report$inputParameters", "report$inputParametersFrame", "report$ReportEntityTree.lookup", "report$ReportExecution.browse", "report$ReportExecution.dialog", "report$ReportGroup.edit", "report$ReportInputParameter.edit", "appPropertiesExport", "propertyConditionFrame", "administration"})
    @Override
    public ScreenPermissionsContainer screenPermissions() {
        return super.screenPermissions();
    }

    @SpecificAccess(permissions = "cuba.gui.filter.customConditions")
    @Override
    public SpecificPermissionsContainer specificPermissions() {
        return super.specificPermissions();
    }
}
