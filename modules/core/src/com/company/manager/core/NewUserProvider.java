package com.company.manager.core;

import com.haulmont.addon.bproc.provider.UserProvider;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.security.entity.User;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component(NewUserProvider.NAME)
public class NewUserProvider implements UserProvider {
    public static final String NAME = "manager_NewUserProvider";

    private static final String MANAGER_ROLE_NAME = "manager";
    private static final String INITIATOR_ROLE_NAME = "initiator";

    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;
    @Inject
    private UserSessionSource userSessionSource;

    @Override
    public User get(String executionId) {
        List<User> managers = dataManager.load(User.class)
                .query("select u from sec$User u join u.userRoles ur where ur.roleName = :roleManager or ur.roleName = :roleInitiator")
                .parameter("roleManager", MANAGER_ROLE_NAME)
                .parameter("roleInitiator", INITIATOR_ROLE_NAME)
                .list();

        if (managers.isEmpty()) // if no manager or initiator user
            throw new RuntimeException("No manager or initiator found");

        User currentUser = userSessionSource.getUserSession().getUser();
        log.info("Current user: " + currentUser.getName());

        // if current user is manager or initiator, assign current user
        for (User user : managers) {
            if (currentUser.getLogin().equals(user.getLogin())) {
                log.info("Assigned user: " + currentUser.getName());
                return currentUser;
            }
        }

        // if current user is not manager or initiator, assign first available
        log.info("Assigned user: " + managers.get(0).getName());
        return managers.get(0);
    }
}