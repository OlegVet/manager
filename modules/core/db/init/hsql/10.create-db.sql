-- begin MANAGER_STATUS
create table MANAGER_STATUS (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CODE varchar(255) not null,
    NAME varchar(255) not null,
    --
    primary key (ID)
)^
-- end MANAGER_STATUS
-- begin MANAGER_ORGANIZATION
create table MANAGER_ORGANIZATION (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    TAX_NUMBER varchar(255) not null,
    REGISTRATION_NUMBER varchar(255) not null,
    ESCAPE_VAT boolean not null,
    --
    primary key (ID)
)^
-- end MANAGER_ORGANIZATION

-- begin MANAGER_CONTRACT
create table MANAGER_CONTRACT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CUSTOMER_ID varchar(36) not null,
    PERFORMER_ID varchar(36) not null,
    NUMBER_ integer not null,
    SIGNED_DATE date not null,
    TYPE_ varchar(50) not null,
    DATE_FROM date not null,
    DATE_TO date not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    CUSTOMER_SIGNER varchar(255) not null,
    PERFORMER_SIGNER varchar(255) not null,
    STATUS varchar(50) not null,
    --
    primary key (ID)
)^
-- end MANAGER_CONTRACT

-- begin MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK
create table MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK (
    CONTRACT_ID varchar(36) not null,
    FILE_DESCRIPTOR_ID varchar(36) not null,
    primary key (CONTRACT_ID, FILE_DESCRIPTOR_ID)
)^
-- end MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK
-- begin MANAGER_STAGE
create table MANAGER_STAGE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    DATE_FROM date not null,
    DATE_TO date not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    DESCRIPTION longvarchar not null,
    CONTRACT_ID varchar(36),
    --
    primary key (ID)
)^
-- end MANAGER_STAGE
-- begin MANAGER_INVOICE
create table MANAGER_INVOICE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    STAGE_BASE_NUMBER_ integer not null,
    STAGE_BASE_DATE_ date not null,
    STAGE_BASE_AMOUNT decimal(19, 2) not null,
    STAGE_BASE_VAT decimal(19, 2) not null,
    STAGE_BASE_TOTAL_AMOUNT decimal(19, 2) not null,
    STAGE_BASE_DESCRIPTION longvarchar not null,
    --
    primary key (ID)
)^
-- end MANAGER_INVOICE
-- begin MANAGER_INVOICE_FILE_DESCRIPTOR_LINK
create table MANAGER_INVOICE_FILE_DESCRIPTOR_LINK (
    INVOICE_ID varchar(36) not null,
    FILE_DESCRIPTOR_ID varchar(36) not null,
    primary key (INVOICE_ID, FILE_DESCRIPTOR_ID)
)^
-- end MANAGER_INVOICE_FILE_DESCRIPTOR_LINK
