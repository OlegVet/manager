-- begin MANAGER_STAGE
alter table MANAGER_STAGE add constraint FK_MANAGER_STAGE_ON_CONTRACT foreign key (CONTRACT_ID) references MANAGER_CONTRACT(ID)^
create index IDX_MANAGER_STAGE_ON_CONTRACT on MANAGER_STAGE (CONTRACT_ID)^
-- end MANAGER_STAGE
-- begin MANAGER_ORGANIZATION
create unique index IDX_MANAGER_ORGANIZATION_UK_REGISTRATION_NUMBER on MANAGER_ORGANIZATION (REGISTRATION_NUMBER) where DELETE_TS is null ^
-- end MANAGER_ORGANIZATION
-- begin MANAGER_CONTRACT
alter table MANAGER_CONTRACT add constraint FK_MANAGER_CONTRACT_ON_CUSTOMER foreign key (CUSTOMER_ID) references MANAGER_ORGANIZATION(ID)^
alter table MANAGER_CONTRACT add constraint FK_MANAGER_CONTRACT_ON_PERFORMER foreign key (PERFORMER_ID) references MANAGER_ORGANIZATION(ID)^
create index IDX_MANAGER_CONTRACT_ON_CUSTOMER on MANAGER_CONTRACT (CUSTOMER_ID)^
create index IDX_MANAGER_CONTRACT_ON_PERFORMER on MANAGER_CONTRACT (PERFORMER_ID)^
-- end MANAGER_CONTRACT
-- begin MANAGER_INVOICE_FILE_DESCRIPTOR_LINK
alter table MANAGER_INVOICE_FILE_DESCRIPTOR_LINK add constraint FK_INVFILDES_ON_INVOICE foreign key (INVOICE_ID) references MANAGER_INVOICE(ID)^
alter table MANAGER_INVOICE_FILE_DESCRIPTOR_LINK add constraint FK_INVFILDES_ON_FILE_DESCRIPTOR foreign key (FILE_DESCRIPTOR_ID) references SYS_FILE(ID)^
-- end MANAGER_INVOICE_FILE_DESCRIPTOR_LINK
-- begin MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK
alter table MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK add constraint FK_CONFILDES_ON_CONTRACT foreign key (CONTRACT_ID) references MANAGER_CONTRACT(ID)^
alter table MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK add constraint FK_CONFILDES_ON_FILE_DESCRIPTOR foreign key (FILE_DESCRIPTOR_ID) references SYS_FILE(ID)^
-- end MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK
-- begin MANAGER_SERV_COMP_CERTIFICATE_ENTITY_FILE_DESCRIPTOR_LINK
alter table MANAGER_SERV_COMP_CERTIFICATE_ENTITY_FILE_DESCRIPTOR_LINK add constraint FK_SERCOMCERENTFILDES_ON_SERV_COMP_CERTIFICATE_ENTITY foreign key (SERV_COMP_CERTIFICATE_ENTITY_ID) references MANAGER_SERV_COMP_CERTIFICATE_ENTITY(ID)^
alter table MANAGER_SERV_COMP_CERTIFICATE_ENTITY_FILE_DESCRIPTOR_LINK add constraint FK_SERCOMCERENTFILDES_ON_FILE_DESCRIPTOR foreign key (FILE_DESCRIPTOR_ID) references SYS_FILE(ID)^
-- end MANAGER_SERV_COMP_CERTIFICATE_ENTITY_FILE_DESCRIPTOR_LINK
-- begin MANAGER_INVOICE
alter table MANAGER_INVOICE add constraint FK_MANAGER_INVOICE_ON_CONTRACT foreign key (CONTRACT_ID) references MANAGER_CONTRACT(ID)^
create index IDX_MANAGER_INVOICE_ON_CONTRACT on MANAGER_INVOICE (CONTRACT_ID)^
-- end MANAGER_INVOICE
-- begin MANAGER_SERV_COMP_CERTIFICATE_ENTITY
alter table MANAGER_SERV_COMP_CERTIFICATE_ENTITY add constraint FK_MANAGER_SERV_COMP_CERTIFICATE_ENTITY_ON_CONTRACT foreign key (CONTRACT_ID) references MANAGER_CONTRACT(ID)^
create index IDX_MANAGER_SERV_COMP_CERTIFICATE_ENTITY_ON_CONTRACT on MANAGER_SERV_COMP_CERTIFICATE_ENTITY (CONTRACT_ID)^
-- end MANAGER_SERV_COMP_CERTIFICATE_ENTITY
