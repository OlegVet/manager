create table MANAGER_CONTRACT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CUSTOMER_ID uuid not null,
    PERFORMER_ID uuid not null,
    NUMBER_ integer not null,
    SIGNED_DATE date not null,
    TYPE_ varchar(50) not null,
    DATE_FROM date not null,
    DATE_TO date not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    CUSTOMER_SIGNER varchar(255) not null,
    PERFORMER_SIGNER varchar(255) not null,
    STATUS varchar(50) not null,
    --
    primary key (ID)
);