create table MANAGER_SERV_COMP_CERTIFICATE_ENTITY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    STAGE_BASE_NUMBER_ integer not null,
    STAGE_BASE_DATE_ date not null,
    STAGE_BASE_AMOUNT decimal(19, 2) not null,
    STAGE_BASE_VAT decimal(19, 2) not null,
    STAGE_BASE_TOTAL_AMOUNT decimal(19, 2) not null,
    STAGE_BASE_DESCRIPTION text not null,
    --
    primary key (ID)
);