alter table MANAGER_INVOICE add constraint FK_MANAGER_INVOICE_ON_CONTRACT foreign key (CONTRACT_ID) references MANAGER_CONTRACT(ID);
create index IDX_MANAGER_INVOICE_ON_CONTRACT on MANAGER_INVOICE (CONTRACT_ID);
