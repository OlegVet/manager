create table MANAGER_CONTRACT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    STAGE_NAME varchar(255) not null,
    STAGE_DATE_FROM date not null,
    STAGE_DATE_TO date not null,
    STAGE_AMOUNT decimal(19, 2) not null,
    STAGE_VAT decimal(19, 2) not null,
    STAGE_TOTAL_AMOUNT decimal(19, 2) not null,
    STAGE_DESCRIPTION longvarchar not null,
    --
    CUSTOMER_ID varchar(36) not null,
    PERFORMER_ID varchar(36),
    NUMBER_ integer not null,
    SIGNED_DATE date not null,
    TYPE_ varchar(50) not null,
    DATE_FROM date not null,
    DATE_TO date not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    CUSTOMER_SIGNER varchar(255) not null,
    PERFORMER_SIGNER varchar(255) not null,
    STATUS varchar(50) not null,
    --
    primary key (ID)
);