create table MANAGER_CONTRACT_STAGE_LINK (
    CONTRACT_ID varchar(36) not null,
    STAGE_ID varchar(36) not null,
    primary key (CONTRACT_ID, STAGE_ID)
);
