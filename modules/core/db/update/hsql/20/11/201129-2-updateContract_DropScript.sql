alter table MANAGER_CONTRACT drop column STAGE_NAME__U71594 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_DATE_FROM__U97802 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_DATE_TO__U84890 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_AMOUNT__U09585 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_VAT__U24594 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_TOTAL_AMOUNT__U85787 cascade ;
alter table MANAGER_CONTRACT drop column STAGE_DESCRIPTION__U06881 cascade ;
