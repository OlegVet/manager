create table MANAGER_STAGE_PROPERTIES (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    DTYPE varchar(31),
    --
    NUMBER_ integer not null,
    DATE_ timestamp not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    DESCRIPTION longvarchar not null,
    --
    primary key (ID)
);