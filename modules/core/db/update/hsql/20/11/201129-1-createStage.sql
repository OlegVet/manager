create table MANAGER_STAGE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    DATE_FROM date not null,
    DATE_TO date not null,
    AMOUNT decimal(19, 2) not null,
    VAT decimal(19, 2) not null,
    TOTAL_AMOUNT decimal(19, 2) not null,
    DESCRIPTION longvarchar not null,
    CONTRACT_ID varchar(36),
    --
    primary key (ID)
);