alter table MANAGER_CONTRACT alter column STAGE_DESCRIPTION rename to STAGE_DESCRIPTION__U06881 ^
alter table MANAGER_CONTRACT alter column STAGE_DESCRIPTION__U06881 set null ;
alter table MANAGER_CONTRACT alter column STAGE_TOTAL_AMOUNT rename to STAGE_TOTAL_AMOUNT__U85787 ^
alter table MANAGER_CONTRACT alter column STAGE_TOTAL_AMOUNT__U85787 set null ;
alter table MANAGER_CONTRACT alter column STAGE_VAT rename to STAGE_VAT__U24594 ^
alter table MANAGER_CONTRACT alter column STAGE_VAT__U24594 set null ;
alter table MANAGER_CONTRACT alter column STAGE_AMOUNT rename to STAGE_AMOUNT__U09585 ^
alter table MANAGER_CONTRACT alter column STAGE_AMOUNT__U09585 set null ;
alter table MANAGER_CONTRACT alter column STAGE_DATE_TO rename to STAGE_DATE_TO__U84890 ^
alter table MANAGER_CONTRACT alter column STAGE_DATE_TO__U84890 set null ;
alter table MANAGER_CONTRACT alter column STAGE_DATE_FROM rename to STAGE_DATE_FROM__U97802 ^
alter table MANAGER_CONTRACT alter column STAGE_DATE_FROM__U97802 set null ;
alter table MANAGER_CONTRACT alter column STAGE_NAME rename to STAGE_NAME__U71594 ^
alter table MANAGER_CONTRACT alter column STAGE_NAME__U71594 set null ;
