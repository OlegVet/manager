-- update MANAGER_CONTRACT set PERFORMER_ID = <default_value> where PERFORMER_ID is null ;
alter table MANAGER_CONTRACT alter column PERFORMER_ID set not null ;
