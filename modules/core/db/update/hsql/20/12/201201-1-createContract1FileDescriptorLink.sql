create table MANAGER_CONTRACT1_FILE_DESCRIPTOR_LINK (
    CONTRACT1_ID varchar(36) not null,
    FILE_DESCRIPTOR_ID varchar(36) not null,
    primary key (CONTRACT1_ID, FILE_DESCRIPTOR_ID)
);
