package com.company.manager.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum ContractStatus implements EnumClass<String> {

    NEW("New"),
    ON_APPROVAL("OnApproval"),
    ACTIVE("Active"),
    COMPLETED("Completed"),
    CANCELED("Canceled");

    private String id;

    ContractStatus(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ContractStatus fromId(String id) {
        for (ContractStatus at : ContractStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}