package com.company.manager.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum ContractType implements EnumClass<String> {

    FIX_PRICE("FixPrice"),
    TIME_AND_MATERIAL("TimeAndMaterial"),
    OUTSTAFF("Outstaff");

    private String id;

    ContractType(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ContractType fromId(String id) {
        for (ContractType at : ContractType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}