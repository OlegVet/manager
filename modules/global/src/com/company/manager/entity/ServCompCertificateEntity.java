package com.company.manager.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Table(name = "MANAGER_SERV_COMP_CERTIFICATE_ENTITY")
@Entity(name = "manager_ServCompCertificateEntity")
@NamePattern("%s|stageBase")
public class ServCompCertificateEntity extends StandardEntity {
    private static final long serialVersionUID = 4980501634972295992L;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
            @AttributeOverride(name = "number", column = @Column(name = "STAGE_BASE_NUMBER_")),
            @AttributeOverride(name = "date", column = @Column(name = "STAGE_BASE_DATE_")),
            @AttributeOverride(name = "amount", column = @Column(name = "STAGE_BASE_AMOUNT")),
            @AttributeOverride(name = "vat", column = @Column(name = "STAGE_BASE_VAT")),
            @AttributeOverride(name = "totalAmount", column = @Column(name = "STAGE_BASE_TOTAL_AMOUNT")),
            @AttributeOverride(name = "description", column = @Column(name = "STAGE_BASE_DESCRIPTION"))
    })
    private StageBase stageBase;

    @JoinTable(name = "MANAGER_SERV_COMP_CERTIFICATE_ENTITY_FILE_DESCRIPTOR_LINK",
            joinColumns = @JoinColumn(name = "SERV_COMP_CERTIFICATE_ENTITY_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILE_DESCRIPTOR_ID"))
    @OnDeleteInverse(DeletePolicy.UNLINK)
    @ManyToMany
    private List<FileDescriptor> files;

    @NotNull
    @OnDeleteInverse(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CONTRACT_ID")
    private Contract contract;

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public List<FileDescriptor> getFiles() {
        return files;
    }

    public void setFiles(List<FileDescriptor> files) {
        this.files = files;
    }

    public StageBase getStageBase() {
        return stageBase;
    }

    public void setStageBase(StageBase stageBase) {
        this.stageBase = stageBase;
    }
}