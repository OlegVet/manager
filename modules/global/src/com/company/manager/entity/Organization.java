package com.company.manager.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Table(name = "MANAGER_ORGANIZATION")
@Entity(name = "manager_Organization")
@NamePattern("%s|name")
public class Organization extends StandardEntity {
    private static final long serialVersionUID = 5245517273152310477L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    @NotEmpty(message = "{msg://manager_Organization.name.validation.NotEmpty}")
    private String name;

    @NotNull
    @Column(name = "TAX_NUMBER", nullable = false)
    @Pattern(message = "{msg://manager_Organization.taxNumber.validation.Pattern}", regexp = "(^\\d{12}$)|(^\\d{10}$)")
    private String taxNumber;

    @NotNull
    @Column(name = "REGISTRATION_NUMBER", nullable = false, unique = true)
    @Length(message = "{msg://manager_Organization.registrationNumber.validation.Length}", min = 13, max = 13)
    private String registrationNumber;

    @NotNull
    @Column(name = "ESCAPE_VAT", nullable = false)
    private Boolean escapeVat = false;

    public Boolean getEscapeVat() {
        return escapeVat;
    }

    public void setEscapeVat(Boolean escapeVat) {
        this.escapeVat = escapeVat;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}