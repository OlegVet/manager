package com.company.manager.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.*;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@PublishEntityChangedEvents
@Table(name = "MANAGER_CONTRACT")
@Entity(name = "manager_Contract")
@NamePattern("%s|number")
@Listeners("manager_ContractEntityListener")
public class Contract extends StandardEntity {
    private static final long serialVersionUID = -4751514068876000752L;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CUSTOMER_ID")
    @OnDeleteInverse(DeletePolicy.UNLINK)
    private Organization customer;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PERFORMER_ID")
    @NotNull
    @OnDeleteInverse(DeletePolicy.UNLINK)
    private Organization performer;

    @NotNull
    @Column(name = "NUMBER_", nullable = false)
    @Positive(message = "{msg://manager_Contract.number.validation.Positive}")
    private Integer number;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "SIGNED_DATE", nullable = false)
    private Date signedDate;

    @NotNull
    @Column(name = "TYPE_", nullable = false)
    private String type;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_FROM", nullable = false)
    private Date dateFrom;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_TO", nullable = false)
    private Date dateTo;

    @NotNull
    @Column(name = "AMOUNT", nullable = false)
    @Positive(message = "{msg://manager_Contract.amount.validation.Positive}")
    @CurrencyValue(currency = "₽")
    private BigDecimal amount;

    @NotNull
    @CurrencyValue(currency = "₽")
    @Column(name = "VAT", nullable = false)
    private BigDecimal vat;

    @NotNull
    @Column(name = "TOTAL_AMOUNT", nullable = false)
    @CurrencyValue(currency = "₽")
    private BigDecimal totalAmount;

    @NotNull
    @Column(name = "CUSTOMER_SIGNER", nullable = false)
    private String customerSigner;

    @NotNull
    @Column(name = "PERFORMER_SIGNER", nullable = false)
    private String performerSigner;

    @NotNull
    @Column(name = "STATUS", nullable = false)
    private String status;

    private @OnDelete(DeletePolicy.CASCADE)
    @Composition
    @OneToMany(mappedBy = "contract")
    List<Stage> stage;

    @OnDelete(DeletePolicy.CASCADE)
    @ManyToMany
    @JoinTable(name = "MANAGER_CONTRACT_FILE_DESCRIPTOR_LINK",
            joinColumns = @JoinColumn(name = "CONTRACT_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILE_DESCRIPTOR_ID"))
    private List<FileDescriptor> files;

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public void setStage(List<Stage> stage) {
        this.stage = stage;
    }

    public List<Stage> getStage() {
        return stage;
    }

    public void setFiles(List<FileDescriptor> files) {
        this.files = files;
    }

    public List<FileDescriptor> getFiles() {
        return files;
    }

    public ContractStatus getStatus() {
        return status == null ? null : ContractStatus.fromId(status);
    }

    public void setStatus(ContractStatus status) {
        this.status = status == null ? null : status.getId();
    }

    public String getPerformerSigner() {
        return performerSigner;
    }

    public void setPerformerSigner(String performerSigner) {
        this.performerSigner = performerSigner;
    }

    public String getCustomerSigner() {
        return customerSigner;
    }

    public void setCustomerSigner(String customerSigner) {
        this.customerSigner = customerSigner;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public ContractType getType() {
        return type == null ? null : ContractType.fromId(type);
    }

    public void setType(ContractType type) {
        this.type = type == null ? null : type.getId();
    }

    public Date getSignedDate() {
        return signedDate;
    }

    public void setSignedDate(Date signedDate) {
        this.signedDate = signedDate;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Organization getPerformer() {
        return performer;
    }

    public void setPerformer(Organization performer) {
        this.performer = performer;
    }

    public Organization getCustomer() {
        return customer;
    }

    public void setCustomer(Organization customer) {
        this.customer = customer;
    }

    @Nullable
    public BigDecimal calculateVat(BigDecimal amount, String vat) {

        if (amount == null) return null;

        boolean escapeVat = false;
        if ((getCustomer() != null) && getCustomer().getEscapeVat())
            escapeVat = true;
        if ((getPerformer() != null) && getPerformer().getEscapeVat())
            escapeVat = true;

        BigDecimal vatInPercents;
        if (escapeVat) {
            vatInPercents = BigDecimal.valueOf(0); // Organization escape vat
        } else {
            try {
                vatInPercents = BigDecimal.valueOf(Integer.parseInt(vat));
            } catch (NumberFormatException ignore) {
                vatInPercents = BigDecimal.valueOf(13); // If vat_in_% in config is not valid number
            }
        }

        return amount.multiply(vatInPercents).divide(BigDecimal.valueOf(100), BigDecimal.ROUND_HALF_UP);
    }

}