package com.company.manager.service;

import com.company.manager.entity.Contract;

public interface ContractStatusService {
    String NAME = "manager_ContractStatusService";

    void setStatus(Contract contract, String status);

}