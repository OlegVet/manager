package com.company.manager.service;

import com.company.manager.entity.Contract;
import com.company.manager.entity.Stage;
import com.haulmont.cuba.core.entity.Entity;

import java.util.List;

public interface StageService {
    String NAME = "manager_StageService";

    /**
     * Create Stage entity using Contract fields.
     * @param contract Target Contract entity in which the Stage will be embedded.
     */
    void create(Contract contract, List<Stage> stagesDc);
}